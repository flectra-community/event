# Flectra Community / event

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[event_project](event_project/) | 2.0.1.0.0| Event project
[event_contact](event_contact/) | 2.0.1.0.0| Add contacts to event and event type
[event_registration_mass_mailing](event_registration_mass_mailing/) | 2.0.1.0.0| Put event registrations emails into mailing lists
[event_session_registration_multi_qty](event_session_registration_multi_qty/) | 2.0.1.0.0| Allow registration grouped by quantities in sessions
[event_mail](event_mail/) | 2.0.1.0.0| Mail settings in events
[website_event_questions_template](website_event_questions_template/) | 2.0.1.0.0| Set question templates for events
[event_sale_registration_multi_qty](event_sale_registration_multi_qty/) | 2.0.1.0.0| Allows sell registrations with more than one attendee
[event_registration_cancel_reason](event_registration_cancel_reason/) | 2.0.1.0.0| Reasons for event registrations cancellations
[event_sale_session](event_sale_session/) | 2.0.1.0.1| Sessions sales in events
[event_session](event_session/) | 2.0.1.0.1| Sessions in events
[partner_event](partner_event/) | 2.0.1.0.1| Link partner to events
[event_registration_multi_qty](event_registration_multi_qty/) | 2.0.1.0.0| Allow registration grouped by quantities
[website_event_require_login](website_event_require_login/) | 2.0.1.0.1| Website Event Require Login


